if [$PROJECT_BRANCH_NAME == "master"]; then export BRANCH_NAME="-$PROJECT_BRANCH_NAME"; else export BRANCH_NAME=""; fi
echo "Building $CI_PROJECT_NAME $PROJECT_NUMBER $BRANCH_NAME"
mkdir /root/.ssh/
echo "${SSH_KEY}" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
touch /root/.ssh/known_hosts
ssh-keyscan mds.nixsolutions.com >> /root/.ssh/known_host