SEARCH_DIRECTORY=$1
FILE_EXTENSION=$2
OVERRIDE_DST_DIR=$3

ssh -o StrictHostKeyChecking=no mds@mds.nixsolutions.com "[ -d "$MDS_PROJECTS_DIR/$PROJECT_NAME/" ] || mkdir $MDS_PROJECTS_DIR/$PROJECT_NAME/"
for file in $(find $SEARCH_DIRECTORY -name "*.$FILE_EXTENSION"); do
    echo "Upload to mds: '$file'"
    filename=$(basename ${file} | cut -f 1 -d '.')
    dstfile=$PROJECT_NAME-$filename-$PROJECT_NUMBER
    dstdir=$([ -z "$OVERRIDE_DST_DIR"] && echo "$(basename $(dirname $file))" || echo "$OVERRIDE_DST_DIR")
    echo "Creating dir '$MDS_PROJECTS_DIR/$PROJECT_NAME/$PROJECT_NUMBER$BRANCH_NAME/$dstdir'"
    ssh mds@mds.nixsolutions.com "mkdir -p '$MDS_PROJECTS_DIR/$PROJECT_NAME/$PROJECT_NUMBER$BRANCH_NAME/$dstdir'"
    scp $file mds@mds.nixsolutions.com:"$MDS_PROJECTS_DIR/$PROJECT_NAME/$PROJECT_NUMBER$BRANCH_NAME/$dstdir/$dstfile.$FILE_EXTENSION"
    echo "Uploading file to: '$MDS_PROJECTS_DIR/$PROJECT_NAME/$PROJECT_NUMBER$BRANCH_NAME/$dstdir/$dstfile.$FILE_EXTENSION'"
    ssh mds@mds.nixsolutions.com "ls -la '$MDS_PROJECTS_DIR/$PROJECT_NAME/$PROJECT_NUMBER$BRANCH_NAME/$dstdir'"
done
ssh mds@mds.nixsolutions.com "chmod 775 -R '$MDS_PROJECTS_DIR/$PROJECT_NAME/$PROJECT_NUMBER$BRANCH_NAME/'"