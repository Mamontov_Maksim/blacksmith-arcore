package com.kpi.ar.blacksmith.ui.activity

import android.os.Bundle
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.mvp.contract.ModelListContract
import com.kpi.ar.blacksmith.mvp.entity.Model3D
import com.kpi.ar.blacksmith.ui.BaseActivity
import com.kpi.ar.blacksmith.ui.adapter.ModelList.ModelAdapter
import kotlinx.android.synthetic.main.activity_model_list.*

class ModelListActivity :
        BaseActivity<ModelListContract.View, ModelListContract.Presenter>(),
        ModelListContract.View {

    override val layoutId = R.layout.activity_model_list

    override val titleToolbarRes = R.string.gallery_3d_models

    private var adapter: ModelAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = ModelAdapter { presenter.onItemClick(it) }
        recyclerView.adapter = adapter

        presenter.loadItems()
    }

    override fun setModelItems(items: List<Model3D>) {
        adapter?.addItems(items)
    }
}