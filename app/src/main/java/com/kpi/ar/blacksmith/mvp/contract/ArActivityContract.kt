package com.kpi.ar.blacksmith.mvp.contract

import com.kpi.ar.blacksmith.mvp.BaseContract

interface ArActivityContract {
    
    interface View : BaseContract.View {

    }
    
    interface Presenter : BaseContract.Presenter<View> {

    }
}