package com.kpi.ar.blacksmith.util.media

object MimeType {
    const val PLAIN_TEXT = "text/plain"
}