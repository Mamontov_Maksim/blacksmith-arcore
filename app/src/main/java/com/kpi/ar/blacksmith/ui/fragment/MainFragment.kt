package com.kpi.ar.blacksmith.ui.fragment

import android.os.Bundle
import android.view.View
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.mvp.contract.MainActivityContract
import com.kpi.ar.blacksmith.ui.BaseFragment
import com.kpi.ar.blacksmith.ui.ToolbarInterface
import com.kpi.ar.blacksmith.ui.adapter.MockAdapter
import com.kpi.ar.blacksmith.ui.toolbar.CollapsingToolbarInterface
import kotlinx.android.synthetic.main.fragment_main.recycler
import javax.inject.Inject

class MainFragment :
        BaseFragment<MainActivityContract.View, MainActivityContract.Presenter>(),
        MainActivityContract.View {

    override val layoutId = R.layout.fragment_main

    override val titleResId = R.string.app_name

    @Inject protected lateinit var toolbarInterface: CollapsingToolbarInterface

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler.adapter = MockAdapter()
    }

    override fun getToolbarInterface(): ToolbarInterface = toolbarInterface

    companion object {

        fun newInstance() = MainFragment()
    }
}
