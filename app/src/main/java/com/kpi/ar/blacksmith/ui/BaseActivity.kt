package com.kpi.ar.blacksmith.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import com.kpi.ar.blacksmith.mvp.BaseContract
import com.kpi.ar.blacksmith.ui.navigation.BaseNavigator
import com.kpi.ar.blacksmith.ui.toolbar.DefaultToolbarInterface
import dagger.android.support.DaggerAppCompatActivity
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Inject

abstract class BaseActivity<V : BaseContract.View, P : BaseContract.Presenter<V>> :
        DaggerAppCompatActivity(),
        BaseContract.View {

    @Inject protected lateinit var presenter: P

    @Inject protected lateinit var navigatorHolder: NavigatorHolder

    @Inject protected lateinit var navigator: BaseNavigator

    @Inject protected lateinit var messageInterface: MessageInterface

    @Inject lateinit var toolbarInterface: DefaultToolbarInterface
        protected set

    @get:LayoutRes protected abstract val layoutId: Int

    @get:StringRes protected abstract val titleToolbarRes: Int

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        presenter.attachView(this as V)
    }

    override fun onResume() {
        super.onResume()
        invalidateToolbar()
        setToolbarTitle()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    final override fun onError(throwable: Throwable) {
        messageInterface.showError(throwable)
    }

    open fun getToolbarInterface(): ToolbarInterface = toolbarInterface

    protected open fun invalidateToolbar() {
        getToolbarInterface().invalidate()
    }

    private fun setToolbarTitle() {
        val title = getString(titleToolbarRes)
        getToolbarInterface().setTitle(title)
    }

    override fun showProgress() {
        // TODO: 10/06/2019  show progress dialog
    }

    override fun hideProgress() {
        // TODO: 10/06/2019  hide progress dialog
    }

    @SuppressLint("ObsoleteSdkInt")
    protected fun checkIsSupportedDeviceOrFinish(activity: Activity): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG)
                    .show()
            activity.finish()
            return false
        }
        val openGlVersionString =
                (activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                        .deviceConfigurationInfo
                        .glEsVersion
        if (openGlVersionString.toDouble() < MIN_OPEN_GL_VERSION) {
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG).show()
            activity.finish()
            return false
        }
        return true
    }

    companion object {

        private const val MIN_OPEN_GL_VERSION = 3.0
    }
}