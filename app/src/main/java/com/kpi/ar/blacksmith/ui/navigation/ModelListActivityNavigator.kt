package com.kpi.ar.blacksmith.ui.navigation

import com.kpi.ar.blacksmith.ui.activity.ModelListActivity
import javax.inject.Inject

class ModelListActivityNavigator @Inject constructor(activity: ModelListActivity) : BaseNavigator(activity)
