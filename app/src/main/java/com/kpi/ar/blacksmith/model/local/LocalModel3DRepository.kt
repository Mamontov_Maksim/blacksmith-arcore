package com.kpi.ar.blacksmith.model.local

import android.content.Context
import com.kpi.ar.blacksmith.model.Model3DRepository
import com.kpi.ar.blacksmith.model.local.entity.Model3DList
import javax.inject.Inject

class LocalModel3DRepository @Inject constructor(
        val context: Context
) : Model3DRepository {

    private val model3DList = Model3DList()

    override fun get3DModelList() = model3DList.get3DModelList()
}