package com.kpi.ar.blacksmith.mvp.presenter

import com.kpi.ar.blacksmith.mvp.contract.ArActivityContract
import com.kpi.ar.blacksmith.di.qualifier.execution.Background
import com.kpi.ar.blacksmith.di.qualifier.execution.Foreground
import com.kpi.ar.blacksmith.mvp.BasePresenter
import io.reactivex.Scheduler
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ArPresenter @Inject constructor(
        router: Router
) : BasePresenter<ArActivityContract.View>(router),
    ArActivityContract.Presenter