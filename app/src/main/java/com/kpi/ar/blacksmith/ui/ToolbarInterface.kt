package com.kpi.ar.blacksmith.ui

import androidx.annotation.LayoutRes
import com.kpi.ar.blacksmith.ui.view.AppBarLayoutStub

interface ToolbarInterface {

    @get:LayoutRes val layoutId: Int

    fun invalidate()

    fun setTitle(title: String)

    fun bindViews(appBarLayoutStub: AppBarLayoutStub)

    fun setVisible(visible: Boolean) {
        TODO("this must be implemented")
    }
}