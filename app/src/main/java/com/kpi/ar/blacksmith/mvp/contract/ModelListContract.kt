package com.kpi.ar.blacksmith.mvp.contract

import com.kpi.ar.blacksmith.mvp.BaseContract
import com.kpi.ar.blacksmith.mvp.entity.Model3D

interface ModelListContract {

    interface View : BaseContract.View {

        fun setModelItems(items: List<Model3D>)
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun onItemClick(model3D: Model3D)

        fun loadItems()
    }
}