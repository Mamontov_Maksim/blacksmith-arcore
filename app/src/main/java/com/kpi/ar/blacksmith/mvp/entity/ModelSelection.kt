package com.kpi.ar.blacksmith.mvp.entity

enum class ModelSelection(val value: String) {
    ANDY("Andy")
}