package com.kpi.ar.blacksmith.mvp.contract

import com.kpi.ar.blacksmith.mvp.BaseContract

interface MainActivityContract {

    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter<View> {

        fun onSettingPageClick()

        fun onProfilePageClick()

        fun onQFormViewerPageClick()

        fun onARCorePageClick()
    }
}