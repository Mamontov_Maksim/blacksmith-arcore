package com.kpi.ar.blacksmith.mvp

import androidx.annotation.CallSuper
import com.kpi.ar.blacksmith.BuildConfig.DEBUG
import com.kpi.ar.blacksmith.mvp.exception.ViewNotAttachedException
import io.reactivex.Scheduler
import ru.terrakok.cicerone.Router
import timber.log.Timber

/**
 * Base class for all presenters.
 *
 * @param <V> view related for presenter implementation.
 */
abstract class BasePresenter<V : BaseContract.View> protected constructor(
        val router: Router
) : BaseContract.Presenter<V> {

    @Suppress("ConstantConditionIf")
    final override var view: V? = null
        get() = field ?: field.apply {
            if (DEBUG) throw ViewNotAttachedException() else Timber.e("View is not attached")
        }
        private set

    override fun onError(throwable: Throwable) {
        if (runCatching { view?.onError(throwable) }.getOrNull() == null) {
            Timber.e(throwable, "Can't show error, view is not attached")
        }
    }

    @CallSuper override fun attachView(view: V) {
        this.view = view
    }

    @CallSuper override fun detachView() {
        this.view = null
    }
}