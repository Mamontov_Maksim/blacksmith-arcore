package com.kpi.ar.blacksmith.util.io

import android.content.Context
import androidx.core.content.FileProvider
import com.kpi.ar.blacksmith.BuildConfig
import com.kpi.ar.blacksmith.util.logger.LogToFileTree
import java.io.File

private val PATTERN = LogToFileTree.LOG_FILE_NAME + "\\d+" + ".log$"

fun Context.getPathToLogFile() =
    cacheDir
        .list()
        .asSequence()
        .filter { Regex(PATTERN).matches(it) }
        .map { File(cacheDir, it) }
        .map { FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, it) }
        .toList()