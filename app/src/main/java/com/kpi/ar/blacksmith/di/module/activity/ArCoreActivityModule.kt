package com.kpi.ar.blacksmith.di.module.activity

import android.app.Activity
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.mvp.contract.ArActivityContract
import com.kpi.ar.blacksmith.mvp.presenter.ArPresenter
import com.kpi.ar.blacksmith.ui.MessageInterface
import com.kpi.ar.blacksmith.ui.activity.ArActivity
import com.kpi.ar.blacksmith.ui.dialog.AlertMessageInterface
import com.kpi.ar.blacksmith.ui.navigation.ArActivityNavigator
import com.kpi.ar.blacksmith.ui.navigation.BaseNavigator
import dagger.Binds
import dagger.Module

@Module interface ArCoreActivityModule {

    @Binds @ActivityScope
    fun bindAlertMessageInterface(messageInterface: AlertMessageInterface): MessageInterface

    @Binds @ActivityScope
    fun bindNavigator(navigator: ArActivityNavigator): BaseNavigator

    @Binds @ActivityScope
    fun bindPresenter(presenter: ArPresenter): ArActivityContract.Presenter

    @Binds @ActivityScope
    fun bindActivity(activity: ArActivity): Activity
}
