package com.kpi.ar.blacksmith.ui.toolbar

import android.app.Activity
import androidx.core.view.isVisible
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.ui.view.AppBarLayoutStub
import kotlinx.android.synthetic.main.toolbar_default.view.toolbar
import javax.inject.Inject

@ActivityScope
class DefaultToolbarInterface @Inject constructor(activity: Activity) :
        BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_default

    override fun setTitle(title: String) {
        toolbar?.title = title
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        toolbar = appBarLayoutStub.toolbar
    }

    override fun setVisible(visible: Boolean) {
        toolbar?.isVisible = visible
    }
}