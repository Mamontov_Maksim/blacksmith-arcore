package com.kpi.ar.blacksmith.model

import com.kpi.ar.blacksmith.model.local.LocalModel3DRepository
import javax.inject.Inject

class ProxyModel3DRepository @Inject constructor(
        val model3DRepository: LocalModel3DRepository
) : Model3DRepository {

    override fun get3DModelList() = model3DRepository.get3DModelList()
}