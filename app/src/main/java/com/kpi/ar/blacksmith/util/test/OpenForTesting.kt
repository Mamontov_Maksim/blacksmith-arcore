package com.kpi.ar.blacksmith.util.test

@Retention(AnnotationRetention.BINARY) annotation class OpenForTesting