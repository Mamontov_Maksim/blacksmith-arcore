package com.kpi.ar.blacksmith.di.component

import android.content.Context
import com.kpi.ar.blacksmith.Application
import com.kpi.ar.blacksmith.di.module.ActivitiesProvider
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivitiesProvider::class
    ]
)
interface AppComponent : AndroidInjector<Application> {

    @Component.Builder interface Builder {

        @BindsInstance fun context(context: Context): Builder

        fun build(): AppComponent
    }
}