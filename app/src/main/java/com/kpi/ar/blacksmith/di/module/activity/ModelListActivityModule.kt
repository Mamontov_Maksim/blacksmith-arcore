package com.kpi.ar.blacksmith.di.module.activity

import android.app.Activity
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.model.ProxyModel3DRepository
import com.kpi.ar.blacksmith.model.Model3DRepository
import com.kpi.ar.blacksmith.mvp.contract.ModelListContract
import com.kpi.ar.blacksmith.mvp.presenter.ModelListPresenter
import com.kpi.ar.blacksmith.ui.MessageInterface
import com.kpi.ar.blacksmith.ui.activity.ModelListActivity
import com.kpi.ar.blacksmith.ui.dialog.AlertMessageInterface
import com.kpi.ar.blacksmith.ui.navigation.BaseNavigator
import com.kpi.ar.blacksmith.ui.navigation.MainActivityNavigator
import com.kpi.ar.blacksmith.ui.navigation.ModelListActivityNavigator
import dagger.Binds
import dagger.Module

@Module interface ModelListActivityModule {

    @Binds @ActivityScope
    fun bindAlertMessageInterface(messageInterface: AlertMessageInterface): MessageInterface

    @Binds @ActivityScope
    fun bindNavigator(navigator: ModelListActivityNavigator): BaseNavigator

    @Binds @ActivityScope
    fun bindRepository(repository: ProxyModel3DRepository): Model3DRepository

    @Binds @ActivityScope
    fun bindPresenter(presenter: ModelListPresenter): ModelListContract.Presenter

    @Binds @ActivityScope
    fun bindActivity(activity: ModelListActivity): Activity
}
