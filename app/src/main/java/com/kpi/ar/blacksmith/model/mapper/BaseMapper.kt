package com.kpi.ar.blacksmith.model.mapper

interface BaseMapper<T, F> {

    fun to(item: T): F

    fun from(item: F): T
}