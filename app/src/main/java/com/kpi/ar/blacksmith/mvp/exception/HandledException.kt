package com.kpi.ar.blacksmith.mvp.exception

class HandledException(message: String, cause: Throwable) : BaseException(message, cause)
