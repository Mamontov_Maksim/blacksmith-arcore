package com.kpi.ar.blacksmith.di.module.arcore

import com.google.ar.sceneform.ux.ArFragment
import dagger.Module
import dagger.Provides

@Module
class ArCoreModule {

    @Provides
    fun provideArSceneView(fragment: ArFragment?) = fragment?.arSceneView

    @Provides
    fun provideTransformationSystem(fragment: ArFragment?) = fragment?.transformationSystem
}