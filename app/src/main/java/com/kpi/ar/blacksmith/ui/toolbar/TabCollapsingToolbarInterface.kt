package com.kpi.ar.blacksmith.ui.toolbar

import android.app.Activity
import com.google.android.material.tabs.TabLayout
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.ui.view.AppBarLayoutStub
import kotlinx.android.synthetic.main.toolbar_tabs_collapsing.view.tabLayout
import kotlinx.android.synthetic.main.toolbar_tabs_collapsing.view.toolbar
import javax.inject.Inject

@ActivityScope
class TabCollapsingToolbarInterface @Inject constructor(activity: Activity) :
    BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_tabs_collapsing

    var tabLayout: TabLayout? = null
        private set

    override fun setTitle(title: String) {
        toolbar?.title = title
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        toolbar = appBarLayoutStub.toolbar
        tabLayout = appBarLayoutStub.tabLayout
    }
}