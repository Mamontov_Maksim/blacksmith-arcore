package com.kpi.ar.blacksmith.ui.adapter.ModelList

import android.view.View
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.mvp.entity.Model3D
import com.kpi.ar.blacksmith.ui.adapter.BaseAdapter

class ModelAdapter(private val onItemClickListener: (Model3D) -> Unit) : BaseAdapter<Model3D>(){

    override fun getLayoutId(viewType: Int) = R.layout.item_recycler_adapter_main

    override fun onCreateViewHolder(view: View, viewType: Int) =
            ModelViewHolder(onItemClickListener, view)
}