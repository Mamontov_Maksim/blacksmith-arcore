package com.kpi.ar.blacksmith

import com.kpi.ar.blacksmith.di.component.DaggerAppComponent
import com.kpi.ar.blacksmith.util.logger.CrashReportingTree
import com.kpi.ar.blacksmith.util.test.OpenForTesting
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber

@OpenForTesting class Application : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        setupTimber()
        setupRxJavaPlugins()
    }

    private fun setupTimber() = Timber.plant(CrashReportingTree(), Timber.DebugTree())

    private fun setupRxJavaPlugins() {
        RxJavaPlugins.setErrorHandler {
            @Suppress("ConstantConditionIf")
            if (BuildConfig.DEBUG) {
                Thread
                    .currentThread()
                    .uncaughtExceptionHandler
                    .uncaughtException(Thread.currentThread(), it)
            } else Timber.e(it, "Error without subscriber")
        }
    }

    override fun applicationInjector(): AndroidInjector<Application> =
        DaggerAppComponent.builder().context(this).build()
}