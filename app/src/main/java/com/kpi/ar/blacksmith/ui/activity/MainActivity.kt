package com.kpi.ar.blacksmith.ui.activity

import android.os.Bundle
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.mvp.contract.MainActivityContract
import com.kpi.ar.blacksmith.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainActivityContract.View, MainActivityContract.Presenter>(),
                     MainActivityContract.View {

    override val layoutId = R.layout.activity_main

    override val titleToolbarRes = R.string.app_name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        settingsPageImage.setOnClickListener {
            presenter.onSettingPageClick()
        }

        profilePageImage.setOnClickListener {
            presenter.onProfilePageClick()
        }

        qFormViewerPageImage.setOnClickListener {
            presenter.onQFormViewerPageClick()
        }

        arCorePageImage.setOnClickListener {
            presenter.onARCorePageClick()
        }
    }

    override fun onResume() {
        super.onResume()
        toolbarInterface.setVisible(false)
    }
}