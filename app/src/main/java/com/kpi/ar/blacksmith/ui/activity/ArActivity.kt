package com.kpi.ar.blacksmith.ui.activity

import android.os.Bundle
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.mvp.contract.ArActivityContract
import com.kpi.ar.blacksmith.mvp.entity.Model3D
import com.kpi.ar.blacksmith.ui.BaseActivity

class ArActivity : BaseActivity<ArActivityContract.View, ArActivityContract.Presenter>(),
                   ArActivityContract.View {

    private var arFragment: ArFragment? = null

    private var andyRenderable: ModelRenderable? = null

    override val layoutId = R.layout.activity_arcore

    override val titleToolbarRes = R.string.empty

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!checkIsSupportedDeviceOrFinish(this)) {
            return
        }

        arFragment = supportFragmentManager.findFragmentById(R.id.uxFragment) as ArFragment

        buildModelRenderable(getModel3D())

        setOnTapArPlaneListener()
    }

    private fun buildModelRenderable(model: Model3D?) {
        if (model == null) throw ExceptionInInitializerError()

        ModelRenderable.builder()
                .setSource(this, model.rawRes)
                .build()
                .thenAccept { renderable: ModelRenderable? -> andyRenderable = renderable }
                .exceptionally {
                    null
                }
    }

    @Suppress("LABEL_NAME_CLASH")
    private fun setOnTapArPlaneListener() {

        arFragment!!.setOnTapArPlaneListener { hitResult, _, _ ->
            if (andyRenderable == null) {
                return@setOnTapArPlaneListener
            }

            val anchor = hitResult.createAnchor()
            val anchorNode = AnchorNode(anchor)
            anchorNode.setParent(arFragment!!.arSceneView.scene)

            val andy = TransformableNode(arFragment!!.transformationSystem)
            andy.setParent(anchorNode)
            andy.renderable = andyRenderable
            andy.select()
        }
    }

    private fun getModel3D(): Model3D? = intent.getParcelableExtra(MODEL_NAME_EXTRA)

    override fun onResume() {
        super.onResume()
        getToolbarInterface().setVisible(false)
    }

    companion object {

        const val MODEL_NAME_EXTRA = "MODEL_NAME_EXTRA"
    }
}