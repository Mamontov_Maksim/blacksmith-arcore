package com.kpi.ar.blacksmith.mvp.presenter

import com.kpi.ar.blacksmith.mvp.BasePresenter
import com.kpi.ar.blacksmith.mvp.contract.MainActivityContract
import com.kpi.ar.blacksmith.ui.navigation.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MainPresenter @Inject constructor(
        router: Router
) : BasePresenter<MainActivityContract.View>(router),
    MainActivityContract.Presenter {

    override fun onSettingPageClick() {
    }

    override fun onProfilePageClick() {
    }

    override fun onQFormViewerPageClick() {
    }

    override fun onARCorePageClick() {
        router.navigateTo(Screens.ModelListActivityScreen())
    }
}