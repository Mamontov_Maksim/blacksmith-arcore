package com.kpi.ar.blacksmith.ui.util.recycler

import androidx.recyclerview.widget.RecyclerView
import com.kpi.ar.blacksmith.util.extensions.hideKeyboard

class HideKeyboardOnScrollListener : RecyclerView.OnScrollListener() {

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        if (RecyclerView.SCROLL_STATE_DRAGGING == newState) recyclerView.hideKeyboard()
    }
}
