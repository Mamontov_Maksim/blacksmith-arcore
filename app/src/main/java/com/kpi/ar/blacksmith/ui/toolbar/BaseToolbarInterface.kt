package com.kpi.ar.blacksmith.ui.toolbar

import android.app.Activity
import androidx.appcompat.widget.Toolbar
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.ui.ToolbarInterface
import com.kpi.ar.blacksmith.ui.view.AppBarLayoutStub

abstract class BaseToolbarInterface(private val activity: Activity) : ToolbarInterface {

    protected var toolbar: Toolbar? = null

    override fun invalidate() {
        var appBarLayoutStubLayout = activity.findViewById<AppBarLayoutStub>(R.id.toolbarViewStub)
        if (appBarLayoutStubLayout.layoutResource != layoutId) {
            appBarLayoutStubLayout.layoutResource = layoutId
            appBarLayoutStubLayout = appBarLayoutStubLayout.inflate()
            bindViews(appBarLayoutStubLayout)
        }
    }
}