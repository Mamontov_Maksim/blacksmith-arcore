package com.kpi.ar.blacksmith.mvp.entity

import android.os.Parcelable
import androidx.annotation.RawRes
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Model3D(
        val name: String,
        val description: String,
        val imageSrc: Int,
        val dataCreated: String,
        @RawRes val rawRes: Int
) : Parcelable {

    companion object {

        fun empty() = Model3D(
                name = "",
                description = "",
                imageSrc = 0,
                dataCreated = "",
                rawRes = 0
        )
    }
}