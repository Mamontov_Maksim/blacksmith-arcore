package com.kpi.ar.blacksmith.ui.toolbar

import android.app.Activity
import android.widget.ImageView
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.ui.view.AppBarLayoutStub
import kotlinx.android.synthetic.main.toolbar_collapsing.view.collapsingToolbar
import kotlinx.android.synthetic.main.toolbar_collapsing.view.toolbarImageView
import javax.inject.Inject

@ActivityScope
class CollapsingToolbarInterface @Inject constructor(activity: Activity) :
    BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_collapsing

    var toolbarImageView: ImageView? = null
        private set

    var collapsingToolbarLayout: CollapsingToolbarLayout? = null
        private set

    override fun setTitle(title: String) {
        collapsingToolbarLayout?.title = title
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        toolbarImageView = appBarLayoutStub.toolbarImageView
        collapsingToolbarLayout = appBarLayoutStub.collapsingToolbar
    }
}