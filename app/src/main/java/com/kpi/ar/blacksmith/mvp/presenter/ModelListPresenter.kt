package com.kpi.ar.blacksmith.mvp.presenter

import com.kpi.ar.blacksmith.model.Model3DRepository
import com.kpi.ar.blacksmith.mvp.BasePresenter
import com.kpi.ar.blacksmith.mvp.contract.ModelListContract
import com.kpi.ar.blacksmith.mvp.entity.Model3D
import com.kpi.ar.blacksmith.ui.navigation.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ModelListPresenter @Inject constructor(
        private val model3DRepository: Model3DRepository,
        router: Router
) : BasePresenter<ModelListContract.View>(router),
    ModelListContract.Presenter {

    override fun loadItems() {
        view?.setModelItems(model3DRepository.get3DModelList())
    }

    override fun onItemClick(model3D: Model3D) {
        router.navigateTo(Screens.ArCoreScreen(model3D))
    }
}