package com.kpi.ar.blacksmith.di.module.activity

import android.app.Activity
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.mvp.contract.MainActivityContract
import com.kpi.ar.blacksmith.mvp.presenter.MainPresenter
import com.kpi.ar.blacksmith.ui.MessageInterface
import com.kpi.ar.blacksmith.ui.activity.MainActivity
import com.kpi.ar.blacksmith.ui.dialog.AlertMessageInterface
import com.kpi.ar.blacksmith.ui.navigation.BaseNavigator
import com.kpi.ar.blacksmith.ui.navigation.MainActivityNavigator
import dagger.Binds
import dagger.Module

@Module interface MainActivityModule {

    @Binds @ActivityScope
    fun bindAlertMessageInterface(messageInterface: AlertMessageInterface): MessageInterface

    @Binds @ActivityScope
    fun bindNavigator(navigator: MainActivityNavigator): BaseNavigator

    @Binds @ActivityScope
    fun bindPresenter(presenter: MainPresenter): MainActivityContract.Presenter

    @Binds @ActivityScope
    fun bindActivity(activity: MainActivity): Activity
}
