package com.kpi.ar.blacksmith.ui.adapter.ModelList

import android.view.View
import com.kpi.ar.blacksmith.mvp.entity.Model3D
import com.kpi.ar.blacksmith.ui.adapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_recycler_adapter_main.view.*

class ModelViewHolder(
        private val onItemClickListener: (Model3D) -> Unit,
        itemView: View
) : BaseViewHolder<Model3D>(itemView){

    private val nameTextView = itemView.modelName
    private val descriptionTextView = itemView.modelDescription
    private val dataCreated = itemView.modelDataCreated
    private val imageView = itemView.modelImage

    init {
        itemView.setOnClickListener {
            onItemClickListener(item)
        }
    }

    override fun bind(item: Model3D) {
        super.bind(item)

        nameTextView.text = item.name
        descriptionTextView.text = item.description
        dataCreated.text = item.dataCreated
        //TODO : 02.01.2020 make load image with Glide
    }
}