package com.kpi.ar.blacksmith.ui.toolbar

import android.app.Activity
import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.ui.view.AppBarLayoutStub
import javax.inject.Inject

@ActivityScope
class WithoutToolbarInterface @Inject constructor(activity: Activity) :
    BaseToolbarInterface(activity) {

    override val layoutId = R.layout.toolbar_without_toolbar

    override fun setTitle(title: String) {
        //none
    }

    override fun bindViews(appBarLayoutStub: AppBarLayoutStub) {
        // none
    }
}