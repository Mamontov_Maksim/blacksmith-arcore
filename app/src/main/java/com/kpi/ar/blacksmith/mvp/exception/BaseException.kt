package com.kpi.ar.blacksmith.mvp.exception

open class BaseException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
