package com.kpi.ar.blacksmith.ui.navigation

import com.kpi.ar.blacksmith.ui.activity.ArActivity
import javax.inject.Inject

class ArActivityNavigator @Inject constructor(activity: ArActivity) : BaseNavigator(activity)
