package com.kpi.ar.blacksmith.model

import com.kpi.ar.blacksmith.mvp.entity.Model3D

interface Model3DRepository {

    fun get3DModelList(): List<Model3D>
}