package com.kpi.ar.blacksmith.model.local.entity

import com.kpi.ar.blacksmith.R
import com.kpi.ar.blacksmith.mvp.entity.Model3D

class Model3DList {

    fun get3DModelList() =
            arrayListOf(
                    Model3D(
                            name = "Andy",
                            description = "Andy : sample for 3D models",
                            imageSrc = R.drawable.ic_android_black,
                            dataCreated = "",
                            rawRes = R.raw.andy
                    )
            )
}