package com.kpi.ar.blacksmith.ui.navigation

import com.kpi.ar.blacksmith.ui.activity.MainActivity
import com.kpi.ar.blacksmith.ui.activity.ModelListActivity
import javax.inject.Inject

class MainActivityNavigator @Inject constructor(activity: MainActivity) : BaseNavigator(activity)