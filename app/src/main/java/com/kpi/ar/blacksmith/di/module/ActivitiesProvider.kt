package com.kpi.ar.blacksmith.di.module

import com.kpi.ar.blacksmith.di.module.activity.ArCoreActivityModule
import com.kpi.ar.blacksmith.di.module.activity.MainActivityModule
import com.kpi.ar.blacksmith.di.module.activity.ModelListActivityModule
import com.kpi.ar.blacksmith.di.module.arcore.ArCoreModule
import com.kpi.ar.blacksmith.di.scope.ActivityScope
import com.kpi.ar.blacksmith.ui.activity.ArActivity
import com.kpi.ar.blacksmith.ui.activity.MainActivity
import com.kpi.ar.blacksmith.ui.activity.ModelListActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module abstract class ActivitiesProvider {

    @ActivityScope
    @ContributesAndroidInjector(
            modules = [
                MainActivityModule::class,
                NavigationModule::class
            ]
    )
    abstract fun contributeMainActivityInjector(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(
            modules = [
                ModelListActivityModule::class,
                NavigationModule::class
            ]
    )
    abstract fun contributeModelListActivityInjector(): ModelListActivity

    @ActivityScope
    @ContributesAndroidInjector(
            modules = [
                ArCoreActivityModule::class,
                NavigationModule::class,
                ArCoreModule::class
            ]
    )
    abstract fun contributeArCoreActivityInjector(): ArActivity
}
